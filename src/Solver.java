/**
 * Created by camilo on 15/01/17.
 *
 * This solver implements the Gaussian elimination method for solving linear systems.
 * It first reduces the matrix to the row echelon form using Partial Pivoting
 * to avoid underflow/overflow and instability of the algorithm. Then the solution vector
 * is founded by performing back substitution.
 *
 * The matrix must be square and the vector must have the same number of elements than rows in the matrix.
 */

public class Solver {
    private Matrix A;   //b,x must have the same size = rows of A
    private Vector b;

    public Solver(Matrix A, Vector b) {
        this.A = A;
        this.b = b;
    }

    public Vector SolveSystem(){
        int rows = b.getSize();     // Number of rows of the matrix
        int pivot;                  // stores the index of the pivot in every iteration
        Vector Row_i = new Vector(rows);        // pivot row
        Vector Row_j = new Vector(rows);        // row being reduced
        double Element_i, Element_j;

        for (int i=0; i<rows-1 ; i++){
            pivot = FindPivot(A.GetColumn(i), i);           // find the pivot in every iteration
            if(pivot != i) A.SwapRows(pivot, i); b.SwapElements(pivot,i);   //partial pivoting
            Row_i.SetVector(A.GetRow(i));
            Element_i = b.GetElement(i);

            // eliminates the pivot variable in all the equations below
            for (int j=i+1; j<rows; j++){
                Row_j.SetVector(A.GetRow(j));
                Element_j = b.GetElement(j);
                //calculates the new element element of the vecor b
                Element_j = Element_j - (Row_j.GetElement(i) / Row_i.GetElement(i)) * Element_i;
                // reduces the j row
                Row_j.SetVector(SubstractRows(Row_j,GetScaledRow(Row_i, Row_j.GetElement(i)/ Row_i.GetElement(i))));
                A.SetRow(j, Row_j);
                b.SetElement(j, Element_j);
            }
        }

        //Back Substitution
        Vector x = new Vector(rows);
        // calculates the last unknown variable
        x.SetElement(rows - 1, (b.GetElement(rows - 1) / A.GetElement(rows -1, rows - 1)));
        // calculates every one of the unknows using the last one x(N-1)
        double sum = 0;
        for (int i=rows-2; i>= 0; i--){
            for(int j=i+1; j<=rows-1; j++){
                sum += x.GetElement(j)*A.GetElement(i,j);
            }
            // adds every unknown to the vector
            x.SetElement(i,((b.GetElement(i) - sum) / A.GetElement(i,i)));
            sum = 0;
        }
        return x;
    }

    // find the pivot on a vector starting looking at the element rowi
    public int FindPivot(Vector z, int rowi){
        double max = z.GetElement(0);
        int index = 0;
        for(int i=rowi; i<z.getSize(); i++){
            if(Math.abs(z.GetElement(i)) > max){
                max = Math.abs(z.GetElement(i));
                index = i;
            }
        }
        return index;
    }

    // retuns a scaled a vector by the factor specified in "scale"
    public Vector GetScaledRow(Vector v, double scale){
        Vector temp = new Vector(v.getSize());
        for(int i=0; i<v.getSize(); i++){
            temp.SetElement(i, scale*v.GetElement(i));
        }
        return temp;
    }

    // return the Vector result of the substraction of the arguments
    public Vector SubstractRows(Vector v, Vector w){
        Vector temp = new Vector(v.getSize());
        for (int i=0; i<v.getSize(); i++){
            temp.SetElement(i,v.GetElement(i) - w.GetElement(i));
        }
        return temp;
    }
}