/**
 * Created by camilo on 14/01/17.
 */
public class Matrix {
    private final int M;    //Number of rows
    private final int N;    //Number of columns
    private final Vector[] the_matrix;      //data

    // constructor with dimensions as argument
    public Matrix(int M, int N) {
        this.M = M;
        this.N = N;
        the_matrix = new Vector[M];
        for (int i=0; i<M; i++) the_matrix[i] = new Vector(N);
    }

    // constructor with multidimensional array as argument
    public Matrix(double[][] the_matrix) {
        M = the_matrix.length;
        N = the_matrix[0].length;
        this.the_matrix = new Vector[M];
        for (int i=0; i<M; i++){
            this.the_matrix[i] = new Vector(N);
            this.the_matrix[i].SetVector(the_matrix[i]);
        }
    }

    // constructor with an array of Vectors as argument
    public Matrix(Vector[] the_matrix) {
        M = the_matrix.length;
        N = the_matrix[0].getSize();
        this.the_matrix = new Vector[M];
        for (int i=0; i<M; i++){
            this.the_matrix[i] = new Vector(N);
            this.the_matrix[i].SetVector(the_matrix[i]);
        }
    }

    // swap the rows i and j of the matrix
    public void SwapRows(int i, int j){
        Vector temp = new Vector(N);
        temp.SetVector(the_matrix[i]);
        the_matrix[i].SetVector(the_matrix[j]);
        the_matrix[j].SetVector(temp);
    }

    // print the matrix
    public void show() {
//        System.out.println();
        for (int i = 0; i < M; i++) {
            the_matrix[i].show();
//            System.out.println();
        }
        System.out.println();
    }

    public int getM() {
        return M;
    }

    public int getN() {
        return N;
    }

    // return a Vector with the elements of the k column
    public Vector GetColumn(int k){
        Vector temp = new Vector(M);
        for (int j=0; j<M; j++){
            temp.SetElement(j,the_matrix[j].GetElement(k));
        }
        return temp;
    }

    //return a Vector with the elements of the k row
    public Vector GetRow(int k){
        Vector temp = new Vector(N);
        temp.SetVector(the_matrix[k]);
        return temp;
    }
    //set a row of the matrix. Vector as the argument with the data
    public void SetRow(int row, Vector v){
        the_matrix[row].SetVector(v);
    }

    // return the elemnt i,j of the matrix
    public double GetElement(int i, int j){
        return the_matrix[i].GetElement(j);
    }
}