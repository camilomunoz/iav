/**
 * Created by camilo on 16/01/17.
 */
public class Vector {
    private final int length;           //length
    private final double[] the_vector;  //data

    // constructor with length as unique argument
    public Vector(int length){
        this.length = length;
        the_vector = new double[length];
        for (int i=0; i< length; i++) the_vector[i] = 0.0;
    }

    // constructor with an array as argument
    public Vector(double[] the_vector){
        length = the_vector.length;
        this.the_vector = the_vector;
    }

    // return size of the vector
    public int getSize(){
        return length;
    }

    // swap element i and j
    public void SwapElements(int i, int j){
        double temp = the_vector[i];
        the_vector[i] = the_vector[j];
        the_vector[j] = temp;
    }

    //return element i
    public double GetElement(int i){
        return the_vector[i];
    }

    //set element i
    public void SetElement(int pos, double num){
        the_vector[pos] = num;
    }

    // assign the whole vector
    public void SetVector(double[] data){
        System.arraycopy(data, 0, the_vector, 0, data.length);
    }

    // assign the whole vector
    public void SetVector(Vector data){
        for (int i=0; i<data.getSize() ;i++) the_vector[i] = data.GetElement(i);
    }

    // return the vector as a Vector object
    public Vector GetVector(){
        return this;
    }

    // returns the vector as an array
    public double[] toArray(){
        return the_vector;
    }

    // print the vector
    public void show(){
        System.out.println();
        for (int i=0; i<the_vector.length; i++)
            System.out.printf("%9.4f ", the_vector[i]);
//        System.out.println("}");
    }
}
