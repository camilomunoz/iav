import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by xingke on 17/01/17.
 */
public class MatrixTest {

    private static final double[][] A = {{16, -6, 4, 1},{1, -8, 1, 1},{16, 2, -4, 1},{9, 8, -3, 1}};
    private static final double[] b = {-36, -64, -4, -64};

    private Matrix ma = new Matrix(A);
    private Vector ve = new Vector(b);

    @Test
    public void swapRows() throws Exception {
        double[][] A = {{1, -1, 0, 0},{0, 1, 1, 0},{0, 0, 1, 2},{2, 0, 0, -3}};

        Vector[] vm = new Vector[A.length];
        for (int i=0; i<A.length; i++){
            vm[i] = new Vector(A[0].length);
            vm[i].SetVector(A[i]);
        }

        Matrix ma = new Matrix(vm);

        ma.SwapRows(1,2);
        assertArrayEquals(A[2], ma.GetRow(1).toArray(), 0.001);
    }

    @Test
    public void getColumn() throws Exception {
        double[] ans = {-6.0, -8.0000, 2.0000, 8.0};
        assertArrayEquals(ans, ma.GetColumn(1).toArray(), 0.001);
    }

    @Test
    public void getRow() throws Exception {
        double[] ans = {1, -8, 1, 1};
        assertArrayEquals(ans, ma.GetRow(1).toArray(), 0.001);
    }

    @Test
    public void setRow() throws Exception {
        ma.SetRow(3,ve);
        assertArrayEquals(b,ma. GetRow(3).toArray(), 0.001);
    }

    @Test
    public void getElement() throws Exception {
        assertEquals(16.0, ma.GetElement(0,0), 0.001);
    }
}