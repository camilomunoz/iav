import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by xingke on 17/01/17.
 */
public class VectorTest {
    private static final double[][] A = {{2, 1, 1},{4, -6, 0},{-2, 7, 2}};
    private static final double[] b = {5, -2, 9};

    private Vector v1 = new Vector(A[1]);
    private Vector v2 = new Vector(b);

    @Test
    public void getElement() throws Exception {
        assertEquals(-6.0, v1.GetElement(1), 0.001);
    }

    @Test
    public void swapElements() throws Exception {
        double[] v = {18.2, -1.3, 4.8};
        Vector ve = new Vector(v);
        ve.SwapElements(1,2);
        assertEquals(-1.3, ve.GetElement(2), 0.001);
        assertEquals(4.8, ve.GetElement(1), 0.001);
    }

    @Test
    public void setElement() throws Exception {
        v2.SetElement(0,-89.4);
        assertEquals(-89.4, v2.GetElement(0), 0.001);
    }

    @Test
    public void setVector_array() throws Exception {
        double[] v = {1, -12, 41.1};
        v2.SetVector(v);
        assertArrayEquals(v, v2.GetVector().toArray(), 0.001);
    }

    @Test
    public void setVector_vector() throws Exception {
        v2.SetVector(v1);
        assertArrayEquals(v1.toArray(), v2.GetVector().toArray(), 0.001);
    }
}