import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by xingke on 17/01/17.
 */
public class SolverTest {

    private static final double[][] A = {{16, -6, 4, 1},{1, -8, 1, 1},{16, 2, -4, 1},{9, 8, -3, 1}};
    private static final double[] b = {-36, -64, -4, -64};

    private Matrix ma = new Matrix(A);
    private Vector ve = new Vector(b);

    Solver solver = new Solver(ma, ve);

    @Test
    public void solveSystem() throws Exception {
        System.out.print("The matrix is: ");
        ma.show();
        System.out.print("The corresponding vector is: ");
        ve.show();
        System.out.print("\nThe solution of the system is: ");
        Vector v = solver.SolveSystem();
        v.show();
        double[] ans = {4.0000, -4.0000, -8.0000, -92.0000};
        assertArrayEquals(ans, v.toArray(), 0.001);
    }

    @Test
    public void FindPivot() throws Exception {
        assertEquals(1, solver.FindPivot(ve,0));
    }

    @Test
    public void GetScaledRow() throws Exception {
        double[] ans = {-72.0, -128.0, -8.0, -128.0};
        assertArrayEquals(ans, solver.GetScaledRow(ve, 2).toArray(), 0.001);
    }

    @Test
    public void SubstractRows() throws Exception {
        Vector v1 = new Vector(A[0]);
        Vector v2 = new Vector(A[1]);
        double[] ans = {15.0, 2.0, 3.0, 0.0};
        assertArrayEquals(ans, solver.SubstractRows(v1, v2).toArray(), 0.001);
    }
}